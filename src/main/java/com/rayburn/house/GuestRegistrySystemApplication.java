package com.rayburn.house;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 
 *
 */
@SpringBootApplication
@RestController
public class GuestRegistrySystemApplication {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(GuestRegistrySystemApplication.class, args);
	}
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping("/greetings")
	public String helloWorld() {
		return "Greetings Rayburn Family!";
	}

}
