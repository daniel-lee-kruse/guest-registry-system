/**
 * 
 */
package com.rayburn.house.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

/**
 * @author dlk01
 *
 */
@Entity
@Table(name="GUESTS")
public class GuestDto {
	@Id
	@GeneratedValue
	@Column(name = "GUEST_ID")
	private Long id;
	/**
	 * @return
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	@NotEmpty(message="error.firstName.empty")
	@Length(max=50)
	@Column(name = "FIRST_NAME")
	private String firstName;
	/**
	 * @return
	 */
	public String getFirstName() {
		return this.firstName;
	}
	/**
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@NotEmpty(message="error.lastName.empty")
	@Length(max=50)
	@Column(name="LAST_NAME")
	private String lastName;
	/**
	 * @return
	 */
	public String getLastName() {
		return this.lastName;
	}
	/**
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@NotEmpty(message="error.address1.empty")
	@Length(max=150)
	@Column(name="ADDRESS_1")
	private String address1;
	/**
	 * @return
	 */
	public String getAddress1() {
		return this.address1;
	}
	/**
	 * @param address1
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	@Length(max=150)
	@Column(name="ADDRESS_2")
	private String address2;
	/**
	 * @return
	 */
	public String getAddress2() {
		return this.address2;
	}
	/**
	 * @param address2
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	@NotEmpty(message="error.city.empty")
	@Length(max=150)
	@Column(name="CITY")
	private String city;
	/**
	 * @return
	 */
	public String getCity() {
		return this.city;
	}
	/**
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	@NotEmpty(message="error.state.empty")
	@Length(max=50)
	@Column(name="STATE")
	private String state;
	/**
	 * @return
	 */
	public String getState() {
		return this.state;
	}
	/**
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	@NotEmpty(message="error.zip.empty")
	@Length(max=10)
	@Column(name="ZIP")
	private String zip;
	/**
	 * @return
	 */
	public String getZip() {
		return this.zip;
	}
	/**
	 * @param zip
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	@NotEmpty(message="error.email.empty")
	@Length(max=80)
	@Column(name="EMAIL")
	private String email;
	/**
	 * @return
	 */
	public String getEmail() {
		return this.email;
	}
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="BUNGALOW_NUM")
	private int bungalowNum;
	/**
	 * @return
	 */
	public int getBungalowNum() {
		return this.bungalowNum;
	}
	/**
	 * @param bungalowNum
	 */
	public void setBungalowNum(int bungalowNum) {
		this.bungalowNum = bungalowNum;
	}
	
}
