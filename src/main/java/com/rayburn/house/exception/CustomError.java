/**
 * 
 */
package com.rayburn.house.exception;

import com.rayburn.house.dto.GuestDto;

/**
 * @author dlk01
 * 
 */
public class CustomError extends GuestDto {
	private final String message;
	
	/**
	 * @param message
	 */
	public CustomError(String message) {
		this.message = message;
	}
	
	/**
	 * @return
	 */
	public String getErrorMessage() {
		return message;
	}
}
