/**
 * 
 */
package com.rayburn.house.exception;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dlk01
 * 
 */
public class FieldErrorDetails {
	private String errorName;
	/**
	 * @return
	 */
	public String getErrorName() {
		return errorName;
	}
	/**
	 * @param errorName
	 */
	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}
	
	private int errorStatus;
	/**
	 * @return
	 */
	public int getErrorStatus() {
		return errorStatus;
	}
	/**
	 * @param errorState
	 */
	public void setErrorStatus(int errorStatus) {
		this.errorStatus = errorStatus;
	}
	
	private String errorDetail;
	/**
	 * @return
	 */
	public String getErrorDetail() {
		return errorDetail;
	}
	/**
	 * @param errorDetail
	 */
	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}
	
	private long errorTimestamp;
	/**
	 * @return
	 */
	public long getErrorTimestamp() {
		return errorTimestamp;
	}
	/**
	 * @param errorTimestamp
	 */
	public void setErrorTimestamp(long errorTimestamp) {
		this.errorTimestamp = errorTimestamp;
	}
	
	private String errorPath;
	/**
	 * @return
	 */
	public String getErrorPath() {
		return errorPath;
	}
	/**
	 * @param errorPath
	 */
	public void setErrorPath(String errorPath) {
		this.errorPath = errorPath;
	}
	
	private String errorMessage;
	/**
	 * @return
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	private Map<String, List<FieldValidationError>> errors = new HashMap<>();
	/**
	 * @return
	 */
	public Map<String, List<FieldValidationError>> getErrors() {
		return errors;
	}
	
}
