/**
 * 
 */
package com.rayburn.house.exception;

import java.awt.TrayIcon.MessageType;

/**
 * @author dlk01
 * 
 */
public class FieldValidationError {
	private String field;
	/**
	 * @return
	 */
	public String getField() {
		return field;
	}
	/**
	 * @param field
	 */
	public void setField(String field) {
		this.field = field;
	}
	
	private String message;
	/**
	 * @return
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	private MessageType type;
	/**
	 * @return
	 */
	public MessageType getType() {
		return type;
	}
	/**
	 * @param type
	 */
	public void setType(MessageType type) {
		this.type = type;
	}
	
}
