/**
 * 
 */
package com.rayburn.house.exception;

import java.awt.TrayIcon.MessageType;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author dlk01
 * 
 */
@ControllerAdvice
public class ValidationHandler {
	private MessageSource messageSource;
	
	/**
	 * @param messageSource
	 */
	@Autowired
	ValidationHandler(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * @param methodNotValidException
	 * @param request
	 * @return
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<FieldErrorDetails> handleError(
			MethodArgumentNotValidException methodNotValidException,
			HttpServletRequest request) {
		
		FieldErrorDetails  fieldErrorDetails = new FieldErrorDetails();
		fieldErrorDetails.setErrorDetail("Field Validation Failed");
		fieldErrorDetails.setErrorMessage(methodNotValidException.getClass().getName());
		fieldErrorDetails.setErrorName("Field Validation Error");
		fieldErrorDetails.setErrorPath(request.getRequestURI());
		fieldErrorDetails.setErrorStatus(HttpStatus.BAD_REQUEST.value());
		
		BindingResult  result = methodNotValidException.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();
		
		for (FieldError error : fieldErrors) {
			FieldValidationError fieldError = manageFieldError(error);
			List<FieldValidationError> fieldValidationErrorsList = 
					fieldErrorDetails.getErrors().get(error.getField());
			if (fieldValidationErrorsList == null) {
				fieldValidationErrorsList = new ArrayList<>();
			}
			fieldValidationErrorsList.add(fieldError);
			fieldErrorDetails.getErrors().put(error.getField(), fieldValidationErrorsList);
		}
		
		return new ResponseEntity<> (fieldErrorDetails, HttpStatus.BAD_REQUEST);
		
	}
	
	private FieldValidationError manageFieldError(final FieldError error) {
		FieldValidationError fieldValidationError = new FieldValidationError();
		
		if(error!=null) {
			Locale currentLocale = LocaleContextHolder.getLocale();
			String msg = messageSource.getMessage(error.getDefaultMessage(), null, currentLocale);
			fieldValidationError.setField(error.getField());
			fieldValidationError.setType(MessageType.ERROR);
			fieldValidationError.setMessage(msg);		
		}
		return fieldValidationError;
		
	}
}
