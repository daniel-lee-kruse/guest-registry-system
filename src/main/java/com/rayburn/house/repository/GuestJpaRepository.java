/**
 * 
 */
package com.rayburn.house.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rayburn.house.dto.GuestDto;

/**
 * @author dlk01
 * 
 */
@Repository
public interface GuestJpaRepository extends JpaRepository<GuestDto, Long> {
	/**
	 * @param lastName
	 * @return
	 */
	GuestDto findByLastName(String lastName);
	
	/**
	 * @param firstName
	 * @param lastName
	 * @return
	 */
	List<GuestDto> findByFirstNameAndLastName(String firstName, String lastName);
	
	/**
	 * @param bungalowNum
	 * @return
	 */
	GuestDto findByBungalowNum(int bungalowNum);
}
