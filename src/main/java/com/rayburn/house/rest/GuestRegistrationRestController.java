/**
 * 
 */
package com.rayburn.house.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rayburn.house.dto.GuestDto;
import com.rayburn.house.exception.CustomError;
import com.rayburn.house.repository.GuestJpaRepository;

/**
 * @author dlk01
 * 
 */
@RestController
@RequestMapping("/api/guest")
public class GuestRegistrationRestController {
	private GuestJpaRepository guestJpaRepository;
	
	/**
	 * @param guestJpaRepository
	 */
	@Autowired
	public void setGuestJpaRepository(GuestJpaRepository guestJpaRepository) {
		this.guestJpaRepository = guestJpaRepository;
	}
	
	/**
	 * @param guest
	 * @return
	 */
	@PostMapping(value="/", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GuestDto> addNewGuest(@Valid @RequestBody final GuestDto guest) {
		// Ensure this guest isn't already in the system.
		if (!guestJpaRepository.findByFirstNameAndLastName(guest.getFirstName(), guest.getLastName()).isEmpty()) {
			return new ResponseEntity<> (new CustomError("Unable to add the new guest. A guest with the name " + guest.getFirstName() + " " + guest.getLastName() + " already exists."), HttpStatus.CONFLICT);
		}
		
		// Ensure no guests are registered in this bungalow.
		if (guestJpaRepository.findByBungalowNum(guest.getBungalowNum()) != null) {
			return new ResponseEntity<> (new CustomError("Unable to add the new guest. A guest is already assigned to bungalow " + guest.getBungalowNum() + "."), HttpStatus.CONFLICT);
		}
		
		guestJpaRepository.save(guest);
		return new ResponseEntity<>(guest, HttpStatus.CREATED);
	}
	
	/**
	 * @param id
	 * @return
	 */
	@GetMapping(value="/{id}")
	public ResponseEntity<GuestDto> getGuestById(@PathVariable(value="id") final Long id) {
		Optional<GuestDto> guest = guestJpaRepository.findById(id);
		if (!guest.isPresent()) {
			return new ResponseEntity<>(new CustomError("Guest with id " + id + " not found."), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(guest.get(), HttpStatus.OK);
	}
	

	/**
	 * @param id
	 * @param guest 
	 * @return
	 */
	@PostMapping(value="/{id}", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GuestDto> updateGuest(@PathVariable(value="id") final Long id, @RequestBody GuestDto guest) {
		Optional<GuestDto> currentGuest = guestJpaRepository.findById(id);
		if (!currentGuest.isPresent()) {
			return new ResponseEntity<>(new CustomError("Unable to update. Guest with id " + id + " not found."), HttpStatus.NOT_FOUND);
		}
		
		currentGuest.get().setFirstName(guest.getFirstName());
		currentGuest.get().setLastName(guest.getLastName());
		currentGuest.get().setAddress1(guest.getAddress1());
		currentGuest.get().setAddress2(guest.getAddress2());
		currentGuest.get().setCity(guest.getCity());
		currentGuest.get().setState(guest.getState());
		currentGuest.get().setZip(guest.getZip());
		currentGuest.get().setEmail(guest.getEmail());
		currentGuest.get().setBungalowNum(guest.getBungalowNum());
		
		guestJpaRepository.saveAndFlush(currentGuest.get());
		
		return new ResponseEntity<>(currentGuest.get(), HttpStatus.OK);
	}
	
	/**
	 * @return
	 */
	@GetMapping(value="/")
	public ResponseEntity<List<GuestDto>> listAllGuests() {
		List<GuestDto> guests = guestJpaRepository.findAll();
		if (guests.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(guests, HttpStatus.OK);
	}
	
	/**
	 * @param id
	 * @return
	 */
	@DeleteMapping(value="/{id}")
	public ResponseEntity<GuestDto> deleteGuest(@PathVariable(value="id") final Long id) {
		Optional<GuestDto> currentGuest = guestJpaRepository.findById(id);
		if (!currentGuest.isPresent()) {
			return new ResponseEntity<>(new CustomError("Unable to delete. Guest with id " + id + " not found."), HttpStatus.NOT_FOUND);
		}
		guestJpaRepository.delete(currentGuest.get());
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
