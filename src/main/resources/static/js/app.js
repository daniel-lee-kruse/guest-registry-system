var app = angular.module('GuestRegistrationSystem', ['ngRoute', 'ngResource']);

app.config(function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    
    $routeProvider.when('/guestRegistration', {
            templateUrl : '/template/guestRegistration.html',
            controller : 'guestRegistrationController'
    }).when('/guestBook', {
            templateUrl : '/template/guestBook.html',
            controller : 'guestBookController'
    }).when('/updateGuest/:id', {
            templateUrl : '/template/updateGuest.html',
            controller: 'guestUpdateController'
    });
});
