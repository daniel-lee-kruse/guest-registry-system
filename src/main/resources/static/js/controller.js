app.controller('guestRegistrationController', function($scope, $http, $location, $route) {
    
    $scope.submitGuestForm = function() {
        $http({
            method : "POST",
            url : 'http://localhost:8080/api/guest/',
            data : $scope.guest,
        }).then(function(response) {
            $location.path("/guestBook");
            $route.reload();
        }, function (errorResponse) {
            $scope.errorMessage = errorResponse.data.errorMessage;
        });
    }
});

app.controller('guestBookController', function($scope, $http, $location, $route) {
    $http({
        method : 'GET',
        url : 'http://localhost:8080/api/guest/',
    }).then(function(response) {
        $scope.guests = response.data;
    });
    
    $scope.deleteGuest = function(guestId) {
        $http({
            method : 'DELETE',
            url : 'http://localhost:8080/api/guest/' + guestId
        }).then(function(response) {
            $location.path("/guestBook");
            $route.reload();
        });
    }
    
    $scope.updateGuest = function(guestId) {
        $location.path("/updateGuest/" + guestId);
    }
});

app.controller('guestUpdateController', function($scope, $http, $location, $routeParams, $route) {
    
    $scope.guestId = $routeParams.id;
    
    $http({
        method : 'GET',
        url : 'http://localhost:8080/api/guest/' + $scope.guestId,
    }).then(function(response) {
        $scope.guest = response.data;
    });
    $scope.submitGuestForm = function() {
        $http({
            method : 'POST',
            url : 'http://localhost:8080/api/guest/' +  $scope.guestId,
            data : $scope.guest,
        }).then(function(response) {
            $location.path("/guestBook");
            $route.reload();
        }, function(errResponse) {
            $scope.errorMessage = "Error while updating guest - Error Message: '" + errResponse.data.errorMessage + "'.";
        });
    }
});
